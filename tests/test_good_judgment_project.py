from datetime import timedelta

import pytest

import good_judgment_project as gjp


@pytest.fixture(scope="module")
def df():
    return gjp.download()


def test_download(df):
    assert len(df) > 0


class TestSelect:
    def test_number_of_forecasts(self, df, min_forecasts=0.25, max_forecasts=0.75):
        df = gjp.select(df, min_forecasts=min_forecasts, max_forecasts=max_forecasts)
        fraction_forecast = df.n_ifps_forecast / df.n_ifps_total
        assert (min_forecasts <= fraction_forecast).all()
        assert (fraction_forecast <= max_forecasts).all()

    def test_time_window(self, df, start_window=60, end_window=30):
        df = gjp.select(df, start_window=start_window, end_window=end_window)
        time_to_close = df.date_to_close - df.timestamp
        assert (timedelta(end_window) <= time_to_close).all()
        assert (time_to_close <= timedelta(start_window)).all()

    def test_keep(self, df):
        keep_all_df = gjp.select(df, keep="all")
        keep_first_df = gjp.select(df, keep="first")
        keep_last_df = gjp.select(df, keep="last")
        assert keep_first_df.timestamp.mean() < keep_all_df.timestamp.mean()
        assert keep_all_df.timestamp.mean() < keep_last_df.timestamp.mean()

        id_variables = ["user_id", "ifp_id", "answer_option"]
        assert not keep_first_df.duplicated(id_variables).any()
        assert not keep_last_df.duplicated(id_variables).any()
