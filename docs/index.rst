.. Good Judgment Project Data documentation main file, created by
   sphinx-quickstart on Mon Nov 12 14:17:27 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Good Judgment Project Data documentation
===========================================

Utilities for working with the Good Judgment Project data.

.. image:: https://gitlab.com/dsbowen/good-judgment-project/badges/main/pipeline.svg
   :target: https://gitlab.com/dsbowen/good-judgment-project/-/commits/main
.. image:: https://gitlab.com/dsbowen/good-judgment-project/badges/main/coverage.svg
   :target: https://gitlab.com/dsbowen/good-judgment-project/-/commits/main
.. image:: https://badge.fury.io/py/good-judgment-project.svg
   :target: https://badge.fury.io/py/good-judgment-project
.. image:: https://img.shields.io/badge/License-MIT-brightgreen.svg
   :target: https://gitlab.com/dsbowen/good-judgment-project/-/blob/main/LICENSE
.. image:: https://mybinder.org/badge_logo.svg
   :target: https://mybinder.org/v2/gl/dsbowen%2Fgood-judgment-project/HEAD?urlpath=lab
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black

Installation
============

.. code-block:: console

   $ pip install good-judgment-project

Quickstart
==========

TODO

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Changelog <changelog>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Citations
=========

.. code-block::

   
   @software(bowen2022good-judgment-project,
      title={ Good Judgment Project Data },
      author={ Bowen, Dillon },
      year={ 2022 },
      url={ https://dsbowen.gitlab.io/good-judgment-project }
   )

Acknowledgements
================

TODO