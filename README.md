# Good Judgment Project Data

[![pipeline status](https://gitlab.com/dsbowen/good-judgment-project/badges/main/pipeline.svg)](https://gitlab.com/dsbowen/good-judgment-project/-/commits/main)
[![coverage report](https://gitlab.com/dsbowen/good-judgment-project/badges/main/coverage.svg)](https://gitlab.com/dsbowen/good-judgment-project/-/commits/main)
[![PyPI version](https://badge.fury.io/py/good-judgment-project.svg)](https://badge.fury.io/py/good-judgment-project)
[![License](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://gitlab.com/dsbowen/good-judgment-project/-/blob/main/LICENSE)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dsbowen%2Fgood-judgment-project/HEAD?urlpath=lab/tree/examples)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Utilities for working with the Good Judgment Project data. [Read the docs](https://dsbowen.gitlab.io/good-judgment-project).
