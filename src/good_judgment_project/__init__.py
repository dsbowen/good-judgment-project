"""Good Judgment Project Data
By Dillon Bowen dsbowen@wharton.upenn.edu
Utilities for working with the Good Judgment Project data."""
from __future__ import annotations

import io
from datetime import timedelta
from typing import Any

import numpy as np
import pandas as pd
import requests

__version__ = "0.0.1"

# Base for GJP download URLs
URL_BASE = "https://dataverse.harvard.edu/api/access/datafile/:persistentId?persistentId=doi:10.7910/DVN/BPCDH5/"
# URL suffix for the individual forecasting problems (IFPs)
IFPS_URL = "L8WZEF"
# URL suffixes for individual forecasts for years 1-4 of the tournament
FORECAST_URLS = "VHXOB2", "69BPE8", "LT1RHH", "VVMXXP"


def download() -> pd.DataFrame:
    """Download the GJP data.

    Returns:
        pd.DataFrame: GJP data.
    """

    def read_csv(url_suffix: str, **kwargs: Any) -> pd.DataFrame:
        """Read a CSV from the Harvard dataverse data deposit.

        Args:
            url_suffix (str): Suffix of the URL where the CSV is located.

        Returns:
            pd.DataFrame: Downloaded data.
        """
        response = requests.get(f"{URL_BASE}{url_suffix}", timeout=60)
        return pd.read_csv(io.BytesIO(response.content), **kwargs)

    # download data
    problems_df = read_csv(IFPS_URL, encoding_errors="ignore")
    forecast_df = pd.concat(
        [read_csv(url_suffix, sep="\t") for url_suffix in FORECAST_URLS]
    )

    # convert columns to datetime
    df = problems_df.drop(columns="q_status").merge(forecast_df, on="ifp_id")
    df.timestamp = pd.to_datetime(df.timestamp)
    df.date_suspend = pd.to_datetime(df.date_suspend, format="%m/%d/%y %H:%M")
    for column in ("date_start", "date_to_close", "date_closed"):
        df[column] = pd.to_datetime(df[column], format="%m/%d/%y")

    # add scores
    df = df.dropna(subset=["viewtime", "date_closed", "timestamp", "value"])
    correct_answer = df.answer_option == df.outcome
    df["squared_error"] = (df.value - correct_answer) ** 2
    df["baseline_squared_error"] = (1 / df.n_opts - correct_answer) ** 2
    # clip forecasts at 0.005 to avoid infinite log loss
    df["log_loss"] = correct_answer * np.log(df.value.clip(5e-3, 1))
    df["baseline_log_loss"] = correct_answer * np.log(1 / df.n_opts)
    gb = df.groupby(["ifp_id", "user_id", "timestamp"])
    df["brier_score"] = gb.squared_error.transform("mean")
    df["baseline_brier_score"] = gb.baseline_squared_error.transform("mean")
    df["log_loss"] = gb.log_loss.transform("mean")
    df["baseline_log_loss"] = gb.baseline_log_loss.transform("mean")

    # add total number of forecasting problems in each year
    # and the number answered by each forecaster
    df["n_ifps_total"] = df.groupby("year").ifp_id.transform("nunique")
    df["n_ifps_forecast"] = df.groupby(["year", "user_id"]).ifp_id.transform("nunique")

    return df


def select(
    df: pd.DataFrame,
    min_forecasts: float = 0,
    max_forecasts: float = np.inf,
    start_window: int = None,
    end_window: int = 0,
    keep: str = "all",
) -> pd.DataFrame:
    """Select observations from the GJP data.

    Args:
        df (pd.DataFrame): GJP data.
        min_forecasts (float): Forecasters who made fewer than ``min_forecasts`` per
            year will be excluded. If ``min_forecasts`` is between 0 and 1, it is
            interpreted as a percent of the total problems available for forecast that
            year. Defaults to 0.
        max_forecasts (float): Forecasters who made more than ``max_forecasts`` per year
            will be excluded. If ``max_forecasts`` is between 0 and 1, it is interpreted
            as a percent of the total problems available to forecast that year. Defaults
            to None.
        start_window (int, optional): Start of the selected time window (number of days
            before the question was scheduled to close). Defaults to None.
        end_window (int, optional): End of the selected time window (number of days
            before the question was scheduled to close). Defaults to 0.
        keep (str, optional): Indicates to keep the "first", "last", or "all" forecasts
            made during the selected time window. Defaults to "all".

    Returns:
        pd.DataFrame: Selected dataframe.
    """
    # filter based on the number of forecasts made
    if 0 <= min_forecasts < 1:
        min_forecasts = min_forecasts * df.n_ifps_total

    if 0 <= max_forecasts < 1:
        max_forecasts = max_forecasts * df.n_ifps_total

    df = df[
        (min_forecasts <= df.n_ifps_forecast) & (df.n_ifps_forecast <= max_forecasts)
    ]

    # keep only values within the start-end window
    df = df[timedelta(abs(end_window)) < df.date_to_close - df.timestamp]
    if start_window is not None:
        df = df[df.date_to_close - df.timestamp < timedelta(abs(start_window))]

    # remove forecasts made after the question was closed or suspended
    df = df[
        df.timestamp < df[["date_closed", "date_to_close", "date_suspend"]].min(axis=1)
    ]

    # keep only the first or last forecast within the window
    if keep != "all":
        df = df.sort_values("timestamp").drop_duplicates(
            subset=["user_id", "ifp_id", "answer_option"], keep=keep
        )

    return df
